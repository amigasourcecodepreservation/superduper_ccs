*********************************************************
*							*
*	SuperDuper...FAST disk duplicator		*
*							*
*	author: George E. Chamberlain			*
*		November 22, 1988			*
*							*
*	Copyright (c) 1988 Central Coast Software	*
*	    268 Bowie Dr, Los Osos, CA 93402		*
*	     All rights reserved, worldwide		*
*							*
*********************************************************

	INCLUDE	"MACROS.ASM"
	INCLUDE "EQUATES.ASM"
	INCLUDE "IO.I"

	XDEF	_SysBase,SysBase

* Externals defined in Amiga.lib, I guess.

	XREF	_CreatePort,_DeletePort,_CreateExtIO,_DeleteExtIO

* Externals in PGU (Patch GiveUnit)

	XREF	PatchGiveUnit,UnPatchGiveUnit

pr_MsgPort	EQU	$5C
ln_Name		EQU	10		;list node name offset
BufferSize	EQU	512*22		;one full cylinder
MaxTracks	EQU	80		;I know...cylinders
DR_GetUnitID	EQU	-30

ACTION_INHIBIT	EQU	31

OutMsg	MACRO	;MSG, no CRLF FLAG
	LEA	\1,A0
	BSR	DISP..
	IFEQ	NARG-1
	BSR	CRLF..
	ENDC
	ENDM

GO:
	MOVE.W	#MaxTracks-1,Tracks	;Initialize number of tracks
	MOVE.L  A7,initialSP		;initial task stack pointer
	MOVEA.L 4,A6
	MOVE.L  A6,SysBase
	JSR	PatchGiveUnit
	LEA     DosName,A1
	MOVEQ   #LIBVER,D0		;********** specify V1.2 lib here
	CALLSYS	OpenLibrary		;OPEN DOS LIBRARY
	MOVE.L  D0,DosBase
	BEQ	EXIT_TO_DOS		;********** wrong version
	ZAPA	A1			;find this task
	CALLSYS	FindTask
	MOVE.L	D0,QB_TASK		;save ptr for msg ports
	CALLSYS	Input,DosBase
	MOVE.L	D0,STDIN
	CALLSYS	Output
	MOVE.L	D0,STDOUT
	BSR	AllocateBuffers
	NOERROR	1$
	OutMsg	NoMem.
	BRA	ABORT_EXIT
1$:	LEA	DRName,A1		;disk resource
	CALLSYS	OpenResource,SysBase
	MOVE.L	D0,DRBase
	BEQ	ABORT_EXIT
MAIN:	OutMsg	Greet.
	BSR	OpenFloppies
ReadSource:
	OutMsg	LoadFirst.,NoCRLF
	BSR	WaitCR
	ERROR	ABORT_EXIT
	MOVE.W	Tracks,D2		;loop counter
	LEA	BufferTable,A2		;ptr to table of buffers
	CLR.L	ByteOffset
	CLR.W	TrackNbr

RLoop:	MOVE.L	(A2)+,ActiveBuffer	;read into here
	BSR	ReadTrack
	ERROR	1$
	DBF	D2,RLoop
	BSR	MotorOff
	OutMsg	LoadNext.,NoCRLF
	BSR	WaitCR
	NOERROR	WriteDest
	IFEQIB	D0,#'R',ReadSource
	BRA	ABORT_EXIT
1$:	BSR	CRLF..
	OutMsg	ReadError.
	BRA.S	ReadSource

WriteDest:
	BSR	HomeDrives		;get them all aligned
	BSR	DisableTD		;disable other TD's
4$:	MOVE.W	Tracks,D2
	LEA	BufferTable,A2
	CLR.L	ByteOffset
	CLR.W	TrackNbr

3$:	MOVE.L	(A2)+,ActiveBuffer
	BSR	WriteTrack
	NOERROR	1$
	BSR	CRLF..
	OutMsg	WriteError.
	BRA.S	2$
1$:	DBF	D2,3$
2$:	BSR	MotorOff
	OutMsg	LoadNext.,NoCRLF
	BSR	WaitCR
	NOERROR	4$
	BSR	EnableTD		;turn TD's back on
	IFEQIB	D0,#'R',ReadSource,L

ABORT_EXIT:
	BSR	CloseFloppies		;just in case
	BSR	FreeBuffers		;make sure we clean up
EXIT_TO_DOS:
	MOVEA.L initialSP,A7
	MOVE.L  DosBase,A1
	IFZL	A1,1$
	CALLSYS	CloseLibrary,SysBase	;CLOSE THE LIBRARIES
1$:	JSR	UnPatchGiveUnit
	ZAP	D0
	RTS				;back to DOS


* Reads next track into active buffer.

ReadTrack:
	BSR	DisplayTrack
	INCW	TrackNbr
	LEA	DF0_PARAMS,A1
	MOVE.L	DrvIOB(A1),A1		;IOBlock
	MOVE.L	#BufferSize,io_Length(A1)
	MOVE.L	ByteOffset,io_Offset(A1)
	MOVE.L	TrackBuffer,io_Data(A1)	;
	MOVE.W	#CMD_READ,io_Command(A1)
	CALLSYS	DoIO,SysBase
	MOVE.B	io_Error(A1),D0
	BEQ.S	1$			;no error
	STC
	RTS
1$:	MOVE.L	TrackBuffer,A0		;source
	MOVE.L	ActiveBuffer,A1		;dest
	MOVE.L	#BufferSize,D0		;number to move
	BSR	FastMove
	ADD.L	#BufferSize,ByteOffset
	RTS
8$:	STC
	RTS

* Writes next track from active buffer.

WriteTrack:
	BSR	DisplayTrack
	INCW	TrackNbr
	MOVE.L	TrackBuffer,A1		;dest
	MOVE.L	ActiveBuffer,A0		;source
	MOVE.L	#BufferSize,D0		;number to move
	BSR	FastMove
	LEA	DF0_PARAMS,A1		;active drive io parameters
	MOVE.L	DrvIOB(A1),A1
	MOVE.W	#TD_FORMAT,io_Command(A1)
	MOVE.L	TrackBuffer,io_Data(A1) ;ptr to track data
	MOVE.L	#BufferSize,io_Length(A1)
	MOVE.L	ByteOffset,io_Offset(A1)
	CALLSYS	DoIO,SysBase	
	MOVE.B	io_Error(A1),D0
	BEQ.S	1$
	STC
	RTS
1$:	ADD.L	#BufferSize,ByteOffset
	RTS

* Moves buffer contents FAST.

FastMove:
	ASR	#5,D0		;divide by 32
	DECW	D0		;for dbf
1$:	MOVE.L	(A0)+,(A1)+
	MOVE.L	(A0)+,(A1)+
	MOVE.L	(A0)+,(A1)+
	MOVE.L	(A0)+,(A1)+
	MOVE.L	(A0)+,(A1)+
	MOVE.L	(A0)+,(A1)+
	MOVE.L	(A0)+,(A1)+
	MOVE.L	(A0)+,(A1)+
	DBF	D0,1$
	RTS

* Open either or both floppy devices, according to selection flags.
* Returns CY=1 on any failure.

OpenFloppies:
	LEA	DF0_PARAMS,A2
	BSR	OpenDrive
	NOERROR	1$
	OutMsg	OpenFlopErr.
	STC
	RTS

1$:	LEA	DF1_PARAMS,A2
	BSR	OpenDrive
	LEA	DF2_PARAMS,A2
	BSR	OpenDrive
	LEA	DF3_PARAMS,A2
	BSR	OpenDrive
	RTS

* Allocates a msg port and an IO block, then opens trackdisk device.
* Drive param table in A2.  Unit number in D0.

OpenDrive:
	ZAP	D0
	MOVE.B	DrvUnit(A2),D0		;get unit number
	MOVE.L	DRBase,A6
	JSR	DR_GetUnitID(A6)	;check out if drive is present and 3.5"
	IFNZL	D0,8$,L			;not present, or not 3.5"
	MOVE.B	DrvUnit(A2),D0
	ADD.B	#'0',D0
	LEA	DF0.,A0
	MOVE.B	D0,2(A0)		;set up unit number in string
	MOVE.L	A0,D1
	CALLSYS	DeviceProc,DosBase
	MOVE.L	D0,DrvProc(A2)
	BEQ.S	8$			;didn't get process id...can't open
	PEA	0
	PEA	0
	JSR	_CreatePort		;allocate msg port
	ADDQ.L	#8,SP
	MOVE.L	D0,DrvPort(A2)
	BEQ.S	8$			;problems
	PEA	IOTD_SIZE
	MOVE.L	D0,-(SP)
	JSR	_CreateExtIO		;allocate and init IOB
	ADDQ.L	#8,SP
	MOVE.L	D0,DrvIOB(A2)
	BEQ.S	8$			;problems
	MOVE.L	D0,A1
	ZAP	D0
	MOVE.B	DrvUnit(A2),D0		;UNIT number
	LEA	TrkDev.,A0
	ZAP	D1
	CALLSYS	OpenDevice,SysBase	;open it
	IFNZL	D0,8$			;bad open
	MOVE.L	DrvIOB(A2),A1
	MOVE.L	io_Unit(A1),DrvPtr(A2)
	BSR	Inhibit			;make ADOS leave drive alone
	RTS
8$:	STC
	RTS

* Closes either or both floppy devices, according to selection flags.

CloseFloppies:
	LEA	DF0_PARAMS,A2		;try to close df0
	BSR.S	CloseDrive
	LEA	DF1_PARAMS,A2		;try to close df0
	BSR.S	CloseDrive
	LEA	DF2_PARAMS,A2		;try to close df0
	BSR.S	CloseDrive
	LEA	DF3_PARAMS,A2		;try to close df0
	BSR.S	CloseDrive
	RTS

* Close and release a single floppy drive.  A2 points to param table.
* Note: drive may never have been allocated.

CloseDrive:
	IFZL	DrvIOB(A2),1$		;drive already closed
	BSR	Enable			;let ADOS get at drive again
	MOVE.L	DrvIOB(A2),A1
	CALLSYS	CloseDevice,SysBase
	MOVE.L	DrvIOB(A2),-(SP)
	JSR	_DeleteExtIO
	ADDQ.L	#4,SP
	CLR.L	DrvIOB(A2)
1$:	MOVE.L	DrvPort(A2),D0
	BEQ.S	9$			;no port
	MOVE.L	D0,-(SP)
	JSR	_DeletePort
	ADDQ.L	#4,SP
	CLR.L	DrvPort(A2)
9$:	RTS

* Keeps AmigaDOS from fooling around with the floppy while we backup.

Enable:
	ZAP	D0
	BRA.S	IECom
Inhibit:
	MOVEQ	#1,D0
IECom:	MOVE.L	D0,PktArg1		;store inhibit/enable code
	MOVEQ	#ACTION_INHIBIT,D0	;packet type
	MOVE.L	DrvProc(A2),A0		;process ID of handler
;	BSR.S	PacketIO
;	RTS

PacketIO:
	MOVE.L	D0,PktType		;save packet type
	MOVE.L	QB_TASK,A4
	LEA	pr_MsgPort(A4),A4	;point to our own port
	MOVE.L	A4,PktPort		;reload reply port
	LEA	Packet,A1
	MOVE.L	#DosPkt,ln_Name(A1)
	CALLSYS	PutMsg,SysBase		;send the packet
	MOVE.L	A4,A0
	CALLSYS	WaitPort		;wait for a reply
	LEA	Packet,A1
	CALLSYS	Remove			;dequeue the packet
	MOVE.L	PktRes1,D0		;response in D0
	RTS

* Turns selected drive motor off.

MotorOff:
	MOVEQ	#CMD_UPDATE,D0
	BSR.S	8$
	MOVEQ	#CMD_CLEAR,D0
	BSR.S	8$
	MOVEQ	#TD_MOTOR,D0		;turn off the motor
8$:	LEA	DF0_PARAMS,A1		;active drive io parameters
	MOVE.L	DrvIOB(A1),A1
	IFZL	A1,9$
	MOVE.W	D0,io_Command(A1)
	ZAP	D0
	MOVE.L	D0,io_Length(A1)
	CALLSYS	DoIO,SysBase
9$:	RTS

AllocateBuffers:
	MOVE.L	#BufferSize,D0		;try to allocate a buffer
	MOVEQ	#MEM_CHIP,D1			;thi one must be chip...
	CALLSYS	AllocMem,SysBase
	MOVE.L	D0,TrackBuffer		;save ptr
	BEQ.S	8$			;oops...
	LEA	BufferTable,A2
	MOVE.W	Tracks,D2		;now allocate 80 track buffers
1$:	MOVE.L	#BufferSize,D0		;try to allocate a buffer
	ZAP	D1			;any kind will do
	CALLSYS	AllocMem,SysBase
	IFZL	D0,8$			;oops...
	MOVE.L	D0,(A2)+		;else add buffer addr to table
	DBF	D2,1$
	RTS
8$:	STC
	RTS

* Free the allocated track buffers on either queue.

FreeBuffers:
	MOVE.L	TrackBuffer,A1
	IFZL	A1,9$
	MOVE.L	#BufferSize,D0
	CALLSYS	FreeMem,SysBase
	MOVE.W	Tracks,D2
	LEA	BufferTable,A2
1$:	MOVE.L	(A2)+,A1
	IFZL	A1,9$
	MOVE.L	#BufferSize,D0
	CALLSYS	FreeMem,SysBase
	DBF	D2,1$
9$:	RTS

* Sends text pointed to by A0 to screen.

DISP..:
	PUSH	D2-D3
	MOVEQ	#-1,D0
1$:	INCL	D0
	TST.B	0(A0,D0.W)
	BNE.S	1$
	MOVE.L	D0,D3
	MOVE.L	A0,D2
	BSR.S	WriteScreen
	POP	D2-D3
	RTS

CRLF..:	MOVEQ	#2,D3
	MOVE.L	#CRLF,D2
WriteScreen:
	MOVE.L	STDOUT,D1
	CALLSYS	Write,DosBase
	RTS

WaitCR:
	BSR.S	GetInput
	IFZB	InputCnt,9$
	MOVE.B	InputBuf,D0
	BCLR	#5,D0		;make upper case
	STC
9$:	RTS

GetInput:
	MOVE.L	STDIN,D1
	MOVE.L	#InputBuf,D2
	MOVEQ	#80,D3
	CALLSYS	Read,DosBase
	DECL	D0		;don't count CR
	MOVE.B	D0,InputCnt	;number of input chars
	RTS

DisplayTrack:
	ZAP	D0
	MOVE.W	TrackNbr,D0
	LEA	TrackNumber.+6,A0
	PUSH	D2-D3
	MOVEQ	#32,D1
	PUSH	D1
	MOVEQ	#2,D3		;number of DIGITS
	ADD.W	D3,A0		;POINT PAST END OF STRING
	MOVE.L	#10,D2		;FOR DIVIDE
1$:	PUSH	D0		;SAVE 32-BIT DIVIDEND
	ZAP	D0
	MOVE.W	(SP)+,D0	;GET UPPER 16-BITS
	DIVU	D2,D0		;DIVIDE NUMBER BY 10
	MOVE.L	D0,D1		;SAVE REMAINDER FOR NEXT DIVISION
	SWAP	D0		;SAVE QUOTIENT FOR UPPER RESULT
	MOVE.W	(SP)+,D1	;GET LOWER WORD
	DIVU	D2,D1		;DIVIDE UPPER REMAINDER AND LOWER WORD
	MOVE.W	D1,D0		;QUOTIENT TO D0
	SWAP	D1		;REMAINDER
	ADD.B	#$30,D1		;MAKE INTO ASCII CHAR
	MOVE.B	D1,-(A0)	;AND STORE INTO BUFFER
	DECW	D3
	BEQ.S	4$		;oops...results too big...bail out
	TST.L	D0		;ANY QUOTIENT LEFT?
	BNE.S	1$		;YES...GO AGAIN
4$:	POP	D0		;GET FILLER CHAR
	IFZB	D3,3$		;no more room...don't fill
	DECW	D3		;ADJUST FOR DBF
2$:	MOVE.B	D0,-(A0)	;STORE FILLER CHAR
	DBF	D3,2$
3$:	MOVEQ	#9,D3
	MOVE.L	#TrackNumber.,D2
	MOVE.L	STDOUT,D1
	CALLSYS	Write,DosBase
	POP	D2-D3
	RTS

HomeDrives:
	LEA	DF0_PARAMS,A2		;try to close df0
	BSR.S	HomeOneDrive
	LEA	DF1_PARAMS,A2		;try to close df0
	BSR.S	HomeOneDrive
	LEA	DF2_PARAMS,A2		;try to close df0
	BSR.S	HomeOneDrive
	LEA	DF3_PARAMS,A2		;try to close df0
	BSR.S	HomeOneDrive
	RTS

HomeOneDrive:
	MOVE.L	DrvIOB(A2),A1
	IFZL	A1,9$
	ZAP	D0
	MOVE.L	D0,io_Offset(A1)
	MOVE.L	TrackBuffer,io_Data(A1)
	MOVE.W	#TD_SEEK,io_Command(A1)
	CALLSYS	DoIO,SysBase
9$:	RTS


DisableTD:
	LEA	DF0_PARAMS,A2		;try to close df0
	BSR.S	1$
	LEA	DF1_PARAMS,A2		;try to close df0
	BSR.S	1$
	LEA	DF2_PARAMS,A2		;try to close df0
	BSR.S	1$
	LEA	DF3_PARAMS,A2		;try to close df0

1$:	MOVE.L	DrvPtr(A2),A3
	IFZL	A3,4$
	IFNZB	DrvUnit(A2),5$		;not DF0
	MOVE.B	#$87,$41(A3)		;make all drives respond to DF0
	RTS
5$:	MOVE.B	#-1,$41(A3)		;clobber the select bits
	MOVE.W	#-1,$4C(A3)		;force current track to be unknown
	MOVE.L	$10(A3),A1		;get task pointer
	MOVE.B	#-127,9(A1)		;set to very low priority
4$:	RTS

EnableTD:
	MOVE.B	#$F7,D3
	LEA	DF0_PARAMS,A2		;try to close df0
	BSR.S	1$
	LEA	DF1_PARAMS,A2		;try to close df0
	BSR.S	1$
	LEA	DF2_PARAMS,A2		;try to close df0
	BSR.S	1$
	LEA	DF3_PARAMS,A2		;try to close df0
1$:	MOVE.L	DrvPtr(A2),A3
	IFZL	A3,2$
	MOVE.B	D3,$41(A3)		;restore drive select bits
	IFZB	DrvUnit(A2),2$		;dont change priority of DF0
	MOVE.L	$10(A3),A1
	MOVE.B	#5,9(A1)		;restore priority
2$:	ROL.B	#1,D3
	RTS

QB_TASK		DC.L	0
_SysBase
SysBase		DC.L	0
DosBase		DC.L	0
DRBase		DC.L	0
initialSP	DC.L	0
STDIN		DC.L	0
STDOUT		DC.L	0
ByteOffset	DC.L	0		;defines cylinder to read/write
TrackBuffer	DC.L	0		;chip buffer for trackdisk use
ActiveBuffer	DC.L	0		;pointer to an entry in table
BufferTable	DCB.L	MaxTracks,0	;array of buffer pointers
Tracks		DC.W	0		;actual max track (debugging)
TrackNbr	DC.W	0		;track number for display
InputCnt	DC.B	0		;count of input chars

NODE    MACRO
        DC.L    0       ;SUCC
        DC.L    0       ;PRED
        DC.B    \1      ;TYPE
        DC.B    \2      ;PRI
        DC.L    \3      ;NAME POINTER
        ENDM

TrkDev.		TEXTZ	'trackdisk.device'
DosName		TEXTZ	'dos.library'
DRName		TEXTZ	'disk.resource'
DF0.		TEXTZ	'DF0:'
TrackNumber.	DC.B	'Track xx',13
Greet.		TEXTZ	<'CCS FAST Disk Duplicator Program'>
LoadFirst.	TEXTZ	<'Load source disk into DF0, then press Return.'>
LoadNext.	TEXTZ	<'Load target disks into floppy drives, then press Return.'>
NoMem.		TEXTZ	<'Not enough free memory - aborting'>
ReadError.	TEXTZ	<'Error reading source disk.'>
WriteError.	TEXTZ	<'Error writing target disk.'>
OpenFlopErr.	TEXTZ	<'Unable to open DF0:'>
CRLF		DC.B	13,10

	CNOP	0,2

DF0_PARAMS
DF0Port		DC.L	0		;DF0's msg port
DF0_IOB		DC.L	0		;DF0's ioblock 1
DF0Proc		DC.L	0		;"id" of handler process
		DC.L	0		;unit pointer
		DC.B	0		;unit

DF1_PARAMS	DC.L	0		;msg port
		DC.L	0		;iob
		DC.L	0		;proc
		DC.L	0		;unit ptr
		DC.B	1		;unit

DF2_PARAMS	DC.L	0		;msg port
		DC.L	0		;iob
		DC.L	0		;proc
		DC.L	0		;unit ptr
		DC.B	2		;unit

DF3_PARAMS	DC.L	0		;msg port
		DC.L	0		;iob
		DC.L	0		;proc
		DC.L	0		;unit ptr
		DC.B	3		;unit

	CNOP	0,4

Packet
PktMsg	NODE	5,0,DosPkt
	DC.L	0
	DC.W	PMsgSize
DosPkt	DC.L	PktMsg
PktPort	DC.L	0
PktType	DC.L	0
PktRes1	DC.L	0
PktRes2	DC.L	0
PktArg1	DC.L	0
PktArg2	DC.L	0
PktArg3	DC.L	0
PktArg4	DC.L	0,0,0,0
PMsgSize EQU	*-PktMsg

InputBuf DS.B	80

	END

