*********************************************************
*							*
*	Quarterback common equates			*
*							*
*	author: George E. Chamberlain			*
*							*
*	Copyright (c) 1987 Central Coast Software	*
*	    268 Bowie Dr, Los Osos, CA 93402		*
*	     All rights reserved, worldwide		*
*********************************************************

LIBVER		EQU	33	;AmigaDOS V1.2 Lib

MN_SIZE		EQU	20

BLUE	EQU	0
WHITE	EQU	1
BLACK	EQU	2
ORANGE	EQU	3

JAM1	EQU	0
JAM2	EQU	1

EXISTING EQU	1005	;AmigaDOS OPEN EXISTING FILE ACCESS MODE
NEW	EQU	1006	;AmigaDOS OPEN NEW FILE ACCESS MODE

TRUE	EQU	-1	;BOOLEAN CONSTANT
FALSE	EQU	0	;DITTO

MEM_PUBLIC	EQU	1	;EITHER CHIP OR FAST
MEM_CHIP	EQU	2	;CHIP MEMORY
MEM_FAST	EQU	4	;NON-CHIP MEMORY
MEM_CLEAR	EQU	$10000	;clear mem

SELECTUP	EQU	$E8
SELECTDN	EQU	$68
MENUUP		EQU	$E9
MENUDN		EQU	$69

ACCESS_READ	EQU	-2
ACCESS_WRITE	EQU	-1

REQ_WD		EQU	280	;STANDARD REQUESTER WIDTH
REQ_HT		EQU	60	;DITTO HEIGHT

IPEN		EQU	15	;OFFSET TO PEN IN IMAGE
TaskPort	EQU	$5C	;offset to task's port in task control block

* ascii char constants

TAB	EQU	9
LF	EQU	10
CR	EQU	13
SPACE	EQU	32

 STRUCTURE	Drv,0
	APTR	DrvPort
	APTR	DrvIOB
	APTR	DrvProc
	APTR	DrvPtr
	BYTE	DrvUnit

