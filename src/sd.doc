

	SuperDuper - the FAST Disk Duplicator

	Copyright (c) 1988 Central Coast Software

		George Chamberlain
		November 22, 1988

This is a very fast floppy disk duplicator program for the Amiga.  
SuperDuper can produce four fully formatted exact copies per minute.
Tests show that the actual copy time is about 52 seconds.  Allowing a
few seconds to change disks, it is realistic to reproduce up to 240
disks per hour, using an unmodified Amiga equipped with three
external 3.5-inch floppy drives.

SuperDuper achieves its high speed by writing to all floppy drives at
the exact same time.  SuperDuper writes to DF0: but all available
floppy drives perform the operation.  This is accomplished by
patching the select bits used by the trackdisk floppy driver to
address DF0:.  By changing these bits to select ALL floppy drives and
by limiting all operations to WRITE and STEP functions, we can force
the other drives to mimic the function performed by DF0:.  This lets
the program produce up to four disks in the time it would normally
take to produce just one disk.

Note:  As of November 22, 1988, there appears to be some problem
which sometimes causes the Amiga to freeze when disks are changed
AFTER SuperDuper has been terminated.  This does not appear to affect
the copies or performance in any way.  It is an annoyance.


